# README #

### What is this repository for? ###

* Quick summary: Popular Purchases API implementation 
* Version: 1.0-SNAPSHOT

### How do I get set up? ###

* Summary of set up: 
Pull Docker image and test it from SoapUI.

* Configuration
Run `docker run -it -p 8080:8080 jeqo/api-recent-purchases:v1.0`

* How to run tests
Open SoapUI and execute Test Cases: 'Existing User' and 'Non Existing User'