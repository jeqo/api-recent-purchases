/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.domain.model;

import java.util.List;

/**
 *
 * @author jeqo
 */
public class PopularPurchase {

    private final Integer id;
    private final String face;
    private final Integer price;
    private final Integer size;
    private final List<String> recent;

    public PopularPurchase(Product product, List<String> recent) {
        this.id = product.getId();
        this.face = product.getFace();
        this.price = product.getPrice();
        this.size = product.getSize();
        this.recent = recent;
    }

    public PopularPurchase(
            Integer id,
            String face,
            Integer price,
            Integer size,
            List<String> recent
    ) {
        this.id = id;
        this.face = face;
        this.price = price;
        this.size = size;
        this.recent = recent;
    }

    public Integer getId() {
        return id;
    }

    public String getFace() {
        return face;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getSize() {
        return size;
    }

    public List<String> getRecent() {
        return recent;
    }

}
