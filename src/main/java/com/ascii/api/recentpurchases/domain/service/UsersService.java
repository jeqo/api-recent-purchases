/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.domain.service;

import com.ascii.api.recentpurchases.domain.model.User;
import java.util.Optional;

/**
 *
 * @author jeqo
 */
public interface UsersService {

    Optional<User> getUser(String username);
}
