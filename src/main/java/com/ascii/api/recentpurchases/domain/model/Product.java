/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.domain.model;

/**
 *
 * @author jeqo
 */
public class Product {

    private Integer id;
    private String face;
    private Integer size;
    private Integer price;

    public Product() {
    }

    public Product(Integer id, String face, Integer size, Integer price) {
        this.id = id;
        this.face = face;
        this.size = size;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public String getFace() {
        return face;
    }

    public Integer getSize() {
        return size;
    }

    public Integer getPrice() {
        return price;
    }

}
