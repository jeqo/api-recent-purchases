/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.domain.service;

import com.ascii.api.recentpurchases.domain.model.Purchase;
import java.util.List;

/**
 *
 * @author jeqo
 */
public interface PurchasesService {

    List<Purchase> findByUser(String user, Integer limit);

    List<Purchase> findByProduct(Integer productId);
}
