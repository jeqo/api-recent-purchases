/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.domain.model;

import java.util.Date;

/**
 *
 * @author jeqo
 */
public class Purchase {

    private Integer id;
    private Integer productId;
    private String username;
    private Date date;

    public Purchase() {
    }

    public Purchase(Integer id, Integer productId, String username, Date date) {
        this.id = id;
        this.productId = productId;
        this.username = username;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public Integer getProductId() {
        return productId;
    }

    public String getUsername() {
        return username;
    }

    public Date getDate() {
        return date;
    }

}
