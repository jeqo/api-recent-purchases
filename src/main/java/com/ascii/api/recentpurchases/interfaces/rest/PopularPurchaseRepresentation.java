/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.interfaces.rest;

import java.util.List;

/**
 *
 * @author jeqo
 */
public class PopularPurchaseRepresentation {

    private Integer id;
    private String face;
    private Integer price;
    private Integer size;
    private List<String> recent;

    public PopularPurchaseRepresentation() {
    }

    public PopularPurchaseRepresentation(
            Integer id,
            String face,
            Integer price,
            Integer size,
            List<String> recent
    ) {
        this.id = id;
        this.face = face;
        this.price = price;
        this.size = size;
        this.recent = recent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<String> getRecent() {
        return recent;
    }

    public void setRecent(List<String> recent) {
        this.recent = recent;
    }

}
