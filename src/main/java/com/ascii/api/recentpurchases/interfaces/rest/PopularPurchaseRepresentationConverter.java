/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.interfaces.rest;

import com.ascii.api.recentpurchases.domain.model.PopularPurchase;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author jeqo
 */
@ApplicationScoped
public class PopularPurchaseRepresentationConverter {

    public List<PopularPurchaseRepresentation> toRepresentationList(
            List<PopularPurchase> popularPurchases
    ) {
        return popularPurchases.stream()
                .map(p -> toRepresentation(p)).collect(Collectors.toList());
    }

    public PopularPurchaseRepresentation toRepresentation(PopularPurchase popularPurchase) {
        return new PopularPurchaseRepresentation(
                popularPurchase.getId(),
                popularPurchase.getFace(),
                popularPurchase.getPrice(),
                popularPurchase.getSize(),
                popularPurchase.getRecent()
        );
    }
}
