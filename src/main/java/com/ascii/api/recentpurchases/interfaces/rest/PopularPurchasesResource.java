/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.interfaces.rest;

import com.ascii.api.recentpurchases.application.PopularPurchasesService;
import com.ascii.api.recentpurchases.application.UserNotFound;
import com.ascii.api.recentpurchases.infrastructure.cache.CacheManager;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;

import javax.inject.Inject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jeqo
 */
@Stateless
@Path("recent_purchases")
public class PopularPurchasesResource {

    @Inject
    PopularPurchasesService popularPurchasesService;

    @Inject
    PopularPurchaseRepresentationConverter representationConverter;

    @Inject
    CacheManager cacheManager;

    @GET
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PopularPurchaseRepresentation> getByUsername(
            @PathParam("username") String username
    ) {
        try {
            List<PopularPurchaseRepresentation> representations = (ArrayList<PopularPurchaseRepresentation>) cacheManager
                    .getCache().get(username);
            if (!Optional.ofNullable(representations).isPresent()) {
                representations = representationConverter.toRepresentationList(
                        popularPurchasesService.getByUsername(username)
                );
                cacheManager.getCache().put(username, representations);
            }
            return (ArrayList<PopularPurchaseRepresentation>) representations;
        } catch (UserNotFound e) {
            throw new WebApplicationException(Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(String.format(
                            "User with username of '%s' was not found",
                            e.getUsername()
                    )).build());
        }
    }
}
