/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.product;

import com.ascii.api.recentpurchases.domain.model.Product;
import com.ascii.api.recentpurchases.domain.service.ProductsService;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jeqo
 */
@Stateless
public class ExternalProductService implements ProductsService {

    @Resource(name = "productsServiceUrl")
    public String productsServiceUrl;

    private final Client jaxrsClient = ClientBuilder.newClient();

    private WebTarget productsResource;

    @PostConstruct
    public void init() {
        productsResource = jaxrsClient.target(productsServiceUrl);
    }

    @Override
    public Optional<Product> getProduct(Integer id) {
        ProductRepresentation productRepresentation = productsResource
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(ProductRepresentation.class);
        return Optional.ofNullable(productRepresentation.getProduct());
    }

}
