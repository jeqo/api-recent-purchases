/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.cache;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;

/**
 *
 * @author jeqo
 */
@ApplicationScoped
public class CacheManager {

    private DefaultCacheManager cacheManager;

    @PostConstruct
    public void init() {
        cacheManager = new DefaultCacheManager();
    }

    public Cache<String, Object> getCache() {
        return cacheManager.getCache();
    }
}
