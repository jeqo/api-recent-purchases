/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.purchase;

import com.ascii.api.recentpurchases.domain.model.Purchase;
import java.util.List;

/**
 *
 * @author jeqo
 */
public class PurchasesRepresentation {

    private List<Purchase> purchases;

    public PurchasesRepresentation() {
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

}
