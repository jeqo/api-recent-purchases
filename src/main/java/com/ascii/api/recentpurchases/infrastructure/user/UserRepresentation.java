/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.user;

import com.ascii.api.recentpurchases.domain.model.User;

/**
 *
 * @author jeqo
 */
public class UserRepresentation {

    private User user;

    public UserRepresentation() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
