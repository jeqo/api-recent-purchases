/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.user;

import com.ascii.api.recentpurchases.domain.model.User;
import com.ascii.api.recentpurchases.domain.service.UsersService;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jeqo
 */
@Stateless
public class ExternalUserService implements UsersService {

    @Resource(name = "usersServiceUrl")
    public String usersServiceUrl;

    private final Client jaxrsClient = ClientBuilder.newClient();

    private WebTarget usersResource;

    @PostConstruct
    public void init() {
        usersResource = jaxrsClient.target(usersServiceUrl);
    }

    @Override
    public Optional<User> getUser(String username) {
        UserRepresentation userRepresentation = usersResource.path(username)
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(UserRepresentation.class);
        return Optional.ofNullable(userRepresentation.getUser());
    }

}
