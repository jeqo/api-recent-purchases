/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.product;

import com.ascii.api.recentpurchases.domain.model.Product;

/**
 *
 * @author jeqo
 */
public class ProductRepresentation {

    private Product product;

    public ProductRepresentation() {
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
