/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.purchase;

import com.ascii.api.recentpurchases.domain.model.Purchase;
import com.ascii.api.recentpurchases.domain.service.PurchasesService;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import javax.ejb.Stateless;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jeqo
 */
@Stateless
public class ExternalPurchasesService implements PurchasesService {

    @Resource(name = "purchasesServiceUrl")
    public String purchasesServiceUrl;

    private final Client jaxrsClient = ClientBuilder.newClient();

    private WebTarget purchasesResource;

    @PostConstruct
    public void init() {
        purchasesResource = jaxrsClient.target(purchasesServiceUrl);
    }

    @Override
    public List<Purchase> findByUser(String username, Integer limit) {
        PurchasesRepresentation purchaseRepresentation = purchasesResource
                .path("by_user").path(username)
                .queryParam("limit", limit)
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(PurchasesRepresentation.class);
        return purchaseRepresentation.getPurchases();
    }

    @Override
    public List<Purchase> findByProduct(Integer productId) {
        PurchasesRepresentation purchaseRepresentation = purchasesResource
                .path("by_product").path(productId.toString())
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(PurchasesRepresentation.class);
        return purchaseRepresentation.getPurchases();
    }

}
