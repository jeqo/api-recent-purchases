/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application.internal;

import com.ascii.api.recentpurchases.application.PopularPurchasesService;
import com.ascii.api.recentpurchases.application.ProductNotFound;
import com.ascii.api.recentpurchases.application.UserNotFound;
import com.ascii.api.recentpurchases.domain.model.PopularPurchase;
import com.ascii.api.recentpurchases.domain.model.Product;
import com.ascii.api.recentpurchases.domain.model.Purchase;
import com.ascii.api.recentpurchases.domain.service.ProductsService;
import com.ascii.api.recentpurchases.domain.service.PurchasesService;
import com.ascii.api.recentpurchases.domain.service.UsersService;
import com.ascii.api.recentpurchases.domain.model.User;

import javax.ejb.Stateless;

import javax.inject.Inject;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author jeqo
 */
@Stateless
public class DefaultPopularPurchasesService implements PopularPurchasesService {

    private static final int USERS_LIMIT = 5;

    @Inject
    PurchasesService purchasesService;
    @Inject
    UsersService usersService;
    @Inject
    ProductsService productsService;

    @Override
    public List<PopularPurchase> getByUsername(String username) throws UserNotFound {
        User user = usersService.getUser(username)
                .orElseThrow(() -> new UserNotFound(username));

        return purchasesService.findByUser(user.getUsername(), USERS_LIMIT)
                .stream()
                .map(p -> {
                    Product product = productsService.getProduct(p.getProductId())
                            .orElseThrow(() -> new ProductNotFound(p.getProductId()));

                    return new PopularPurchase(
                            product,
                            purchasesService.findByProduct(p.getProductId()).stream()
                            .map(Purchase::getUsername)
                            .collect(Collectors.toList())
                    );
                })
                .sorted((p1, p2) -> Integer.compare(p2.getRecent().size(), p1.getRecent().size()))
                .collect(Collectors.toList());
    }

}
