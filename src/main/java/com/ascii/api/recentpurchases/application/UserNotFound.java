/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application;

/**
 *
 * @author jeqo
 */
public class UserNotFound extends Exception {

    private final String username;

    public UserNotFound(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

}
