/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application;

import com.ascii.api.recentpurchases.domain.model.PopularPurchase;
import java.util.List;

/**
 * Popular Purchases API Definition
 *
 * @author jeqo
 */
public interface PopularPurchasesService {

    /**
     * Get 5 most recent popular purchases
     *
     * @param username
     * @return List of PopularPurchase
     */
    List<PopularPurchase> getByUsername(String username) throws UserNotFound;
}
