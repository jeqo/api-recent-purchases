/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application;

/**
 *
 * @author jeqo
 */
public class ProductNotFound extends RuntimeException {

    private final Integer productId;

    public ProductNotFound(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductId() {
        return productId;
    }

}
