/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.user;

import com.ascii.api.recentpurchases.domain.model.User;

import java.util.Optional;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jeqo
 */
public class ExternalUserServiceIT {

    ExternalUserService userService;

    public ExternalUserServiceIT() {
    }

    @Before
    public void setUp() {
        userService = new ExternalUserService();
        userService.usersServiceUrl = "http://74.50.59.155:6000/api/users";
        userService.init();
    }

    /**
     * Test of getUser method, of class ExternalUserService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void GetUser_ExistingUser_PrintUser() throws Exception {
        System.out.println("GetUser_ExistingUser_PrintUser");
        String username = "Kaylee_Keeling";

        Optional<User> result = userService.getUser(username);
        System.out.println(result.get().getEmail());

        assertNotNull(result.get());
    }

}
