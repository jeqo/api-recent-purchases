/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.product;

import com.ascii.api.recentpurchases.domain.model.Product;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jeqo
 */
public class ExternalProductServiceIT {

    ExternalProductService productService;

    public ExternalProductServiceIT() {
    }

    @Before
    public void setUp() {
        productService = new ExternalProductService();
        productService.productsServiceUrl = "http://74.50.59.155:6000/api/products";
        productService.init();
    }

    /**
     * Test of getProduct method, of class ExternalProductService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void GetProduct_ExistingProduct_PrintFace() throws Exception {
        System.out.println("getProduct");
        Integer id = 31257;

        Optional<Product> result = productService.getProduct(id);
        System.out.println("Product Face: " + result.get().getFace());
        Assert.assertNotNull(result.get());
    }

}
