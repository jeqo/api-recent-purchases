/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.infrastructure.purchase;

import com.ascii.api.recentpurchases.domain.model.Purchase;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jeqo
 */
public class ExternalPurchasesServiceIT {

    ExternalPurchasesService purchasesService;

    @Before
    public void setUp() {
        purchasesService = new ExternalPurchasesService();
        purchasesService.purchasesServiceUrl = "http://74.50.59.155:6000/api/purchases";
        purchasesService.init();
    }

    /**
     * Test of findByUser method, of class ExternalPurchasesService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void FindByUser_ExistingUser_HasPurchases() throws Exception {
        System.out.println("FindByUser_ExistingUser_HasPurchases");
        String username = "Kaylee_Keeling";

        List<Purchase> result = purchasesService.findByUser(username, 5);

        result.forEach(r -> System.out.println(r.getId()));
        assertTrue(result.size() > 0);
    }

    /**
     * Test of findByProduct method, of class ExternalPurchasesService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void FindByProduct_ExistingProduct_HasPurchases() throws Exception {
        System.out.println("FindByProduct_ExistingProduct_HasPurchases");
        Integer productId = 31257;

        List<Purchase> result = purchasesService.findByProduct(productId);

        result.forEach(r -> System.out.println(r.getId()));
        assertTrue(result.size() > 0);
    }

}
