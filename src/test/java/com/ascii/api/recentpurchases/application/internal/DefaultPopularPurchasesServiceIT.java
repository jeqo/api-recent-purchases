/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application.internal;

import com.ascii.api.recentpurchases.domain.model.PopularPurchase;
import com.ascii.api.recentpurchases.infrastructure.product.ExternalProductService;
import com.ascii.api.recentpurchases.infrastructure.purchase.ExternalPurchasesService;
import com.ascii.api.recentpurchases.infrastructure.user.ExternalUserService;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jeqo
 */
public class DefaultPopularPurchasesServiceIT {

    DefaultPopularPurchasesService popularPurchasesService;

    ExternalProductService productService;
    ExternalPurchasesService purchasesService;
    ExternalUserService usersService;

    public DefaultPopularPurchasesServiceIT() {
    }

    @Before
    public void setUp() {
        productService = new ExternalProductService();
        productService.productsServiceUrl = "http://74.50.59.155:6000/api/products";
        productService.init();

        purchasesService = new ExternalPurchasesService();
        purchasesService.purchasesServiceUrl = "http://74.50.59.155:6000/api/purchases";
        purchasesService.init();

        usersService = new ExternalUserService();
        usersService.usersServiceUrl = "http://74.50.59.155:6000/api/users";
        usersService.init();

        popularPurchasesService = new DefaultPopularPurchasesService();
        popularPurchasesService.productsService = productService;
        popularPurchasesService.purchasesService = purchasesService;
        popularPurchasesService.usersService = usersService;
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getByUsername method, of class DefaultPopularPurchasesService.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void GetByUsername_ExistingUser_OrderedPopularPurchases() throws Exception {
        System.out.println("GetByUsername_ExistingUser_OrderedPopularPurchases");
        String username = "Kaylee_Keeling";

        List<PopularPurchase> result = popularPurchasesService.getByUsername(username);
        result.stream()
                .forEach(p -> System.out.println(p.getId() + " - " + p.getRecent().size()));

        assertEquals(5, result.size());
        assertTrue(result.get(0).getRecent().size() > result.get(4).getRecent().size());
    }

}
