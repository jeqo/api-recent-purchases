/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascii.api.recentpurchases.application.internal;

import com.ascii.api.recentpurchases.application.UserNotFound;
import com.ascii.api.recentpurchases.domain.model.PopularPurchase;
import com.ascii.api.recentpurchases.domain.model.Product;
import com.ascii.api.recentpurchases.domain.model.Purchase;
import com.ascii.api.recentpurchases.domain.model.User;
import com.ascii.api.recentpurchases.domain.service.ProductsService;
import com.ascii.api.recentpurchases.domain.service.PurchasesService;
import com.ascii.api.recentpurchases.domain.service.UsersService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 *
 * @author jeqo
 */
public class PopularPurchasesServiceTest {

    ProductsService mockProductsService;
    PurchasesService mockPurchasesService;
    UsersService mockUsersService;

    DefaultPopularPurchasesService popularPurchasesService;

    @Before
    public void setUp() {
        mockProductsService = mock(ProductsService.class);
        mockPurchasesService = mock(PurchasesService.class);
        mockUsersService = mock(UsersService.class);
    }

    /**
     * Test of getByUsername method, of class PopularPurchasesService. Given
     * that user 'jeqo' exists And has recent purchases When I ask to get recent
     * purchase by 'jeqo' username Then I should have a list of ordered purchase
     * list
     *
     * @throws com.ascii.api.recentpurchases.application.UserNotFound
     */
    @Test
    public void GetByUsername_ExistingUser_OrderedPopularPurchases() throws UserNotFound {
        when(mockUsersService.getUser(anyString()))
                .thenReturn(
                        Optional.of(new User("jeqo", "quilcate.jorge@gmail.com"))
                );

        when(mockPurchasesService.findByUser("jeqo", 5))
                .thenReturn(Arrays.asList(
                        new Purchase(1, 5, "abc", new Date()),
                        new Purchase(2, 4, "xyz", new Date()),
                        new Purchase(3, 3, "aaa", new Date()),
                        new Purchase(4, 2, "bbb", new Date()),
                        new Purchase(5, 1, "aee", new Date())
                ));

        when(mockPurchasesService.findByProduct(1))
                .thenReturn(Arrays.asList(
                        new Purchase(5, 1, "aee", new Date())
                ));
        when(mockPurchasesService.findByProduct(2))
                .thenReturn(Arrays.asList(
                        new Purchase(4, 2, "bbb", new Date()),
                        new Purchase(8, 2, "acc", new Date()),
                        new Purchase(9, 2, "acd", new Date())
                ));
        when(mockPurchasesService.findByProduct(3))
                .thenReturn(Arrays.asList(
                        new Purchase(3, 3, "axa", new Date()),
                        new Purchase(11, 3, "xcd", new Date())
                ));
        when(mockPurchasesService.findByProduct(4))
                .thenReturn(Arrays.asList(
                        new Purchase(2, 4, "xdf", new Date())
                ));
        when(mockPurchasesService.findByProduct(5))
                .thenReturn(Arrays.asList(
                        new Purchase(1, 5, "trw", new Date()),
                        new Purchase(62, 5, "trw", new Date()),
                        new Purchase(63, 5, "trw", new Date()),
                        new Purchase(65, 5, "trw", new Date())
                ));

        when(mockProductsService.getProduct(1))
                .thenReturn(Optional.of(new Product(1, ":)", 123, 321)));
        when(mockProductsService.getProduct(2))
                .thenReturn(Optional.of(new Product(2, "{:)", 123, 321)));
        when(mockProductsService.getProduct(3))
                .thenReturn(Optional.of(new Product(3, "}:)", 123, 321)));
        when(mockProductsService.getProduct(4))
                .thenReturn(Optional.of(new Product(4, "*:)", 123, 321)));
        when(mockProductsService.getProduct(5))
                .thenReturn(Optional.of(new Product(5, "-:)", 123, 321)));

        popularPurchasesService = new DefaultPopularPurchasesService();
        popularPurchasesService.productsService = mockProductsService;
        popularPurchasesService.purchasesService = mockPurchasesService;
        popularPurchasesService.usersService = mockUsersService;

        List<PopularPurchase> result = popularPurchasesService.getByUsername("jeqo");
        result.stream()
                .forEach(p -> System.out.println(p.getId() + " - " + p.getRecent().size()));

        assertEquals(5, result.size());
        assertTrue(result.get(0).getRecent().size() > result.get(4).getRecent().size());

    }

    /**
     * Given that user 'jeqo2' does not exists When I ask to get recent purchase
     * by 'jeqo2' username Then I should receive a UserNotFoundException
     *
     * @throws com.ascii.api.recentpurchases.application.UserNotFound
     */
    @Test(expected = UserNotFound.class)
    public void GetByUsername_NonExistingUser_UserNotFoundException() throws UserNotFound {
        User u = null;
        when(mockUsersService.getUser("jeqo2"))
                .thenReturn(Optional.ofNullable(u));

        popularPurchasesService = new DefaultPopularPurchasesService();
        popularPurchasesService.usersService = mockUsersService;
        popularPurchasesService.getByUsername("jeqo2");
    }

}
